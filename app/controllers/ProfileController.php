<?php

namespace App\Controllers;

/**
 * Display the message and conditions page.
 */
class ProfileController extends ControllerBase
{

    public function indexAction()
    {
        $this->view->url = $_SERVER["REQUEST_URI"];
        $this->view->data = $this->dispatcher->getParam('user');
    }

    public function friendsAction()
    {
        $this->view->url = $_SERVER["REQUEST_URI"];
        $this->view->user = $this->dispatcher->getParam('user');
    }

}
