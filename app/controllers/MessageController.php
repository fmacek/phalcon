<?php

namespace App\Controllers;

/**
 * Display the message and conditions page.
 */
class MessageController extends ControllerBase
{

    public function indexAction()
    {
        $this->view->data = $this->dispatcher->getParam('user');
    }
    public function listAction()
    {
        $this->view->data = $this->dispatcher->getParam('user');
    }
}
