<?php
/*
 * Define custom routes. File gets included in the router service definition.
 */
$router = new Phalcon\Mvc\Router();

$router->add('/confirm/{code}/{email}', [
    'controller' => 'user_control',
    'action' => 'confirmEmail'
]);

$router->add('/reset-password/{code}/{email}', [
    'controller' => 'user_control',
    'action' => 'resetPassword'
]);
$router->add('/message', [
    'controller' => 'message',
    'action' => 'list',
]);
$router->add('/message/{user}', [
    'controller' => 'message',
    'action' => 'index',
]);
$router->add('/profile/{user}', [
    'controller' => 'profile',
    'action' => 'index',
]);$router->add('/profile/{user}/:action', [
    'controller' => 'profile',
    'action' => 2,
]);
return $router;
