<div class="row">
    <div class="col-md-3">
        <img src="https://vistana-web-static.s3.amazonaws.com/vistana-web/assets/img/profile/production/profile-pic-medium.png?1538246490"
             class="img-fluid rounded-circle" style="width: 100%">
    </div>
    <div class="col-md-9">
        <div class="row">
            <div class="col-md-12">
                <h2>Filip Macek</h2>
            </div>
            <div class="col-md-12">
                <h6>„Lorem ipsum dolor sit amet!“</h6>
            </div>

        </div>

        <div class="row">
            <div class="col-md-5">

                <table class="table table-sm">

                    <tbody>
                    <tr>
                        <th scope="row">Pohlaví</th>
                        <td>Muž</td>
                    </tr>
                    <tr>
                        <th scope="row">Věk</th>
                        <td>21 let</td>
                    </tr>
                    <tr>
                        <th scope="row">Kraj</th>
                        <td>Hlavní město Praha</td>
                    </tr>
                    <tr>
                        <th scope="row">Stát</th>
                        <td>Česká Republika</td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="col-md-5">
                <button class="btn btn-primary">
                    <span class="fa fa-user-plus"></span> Přidat do přátel
                </button>
                <br>
                <button class="btn btn-danger" style="margin-top: 5px">
                    <span class="fa fa-ban"></span> Přidat do ignorace
                </button>
            </div>

        </div>
    </div>
</div>
{% set tabs=[
    '/profile/fmacek' : ['text' : 'Zeď', 'icon' : 'receipt'],
    '/profile/fmacek/friends' : ['text' : 'Přátelé', 'icon' : 'users'],
    '/message/fmacek' : ['text' : 'Zprávy', 'icon' : 'comments']
] %}

<div class="row" style="margin-top: 20px;">
    <div class="col-md-12">
        <ul class="nav nav-tabs">
            {% for link, text in tabs %}
            <li class="nav-item">
                {% if link == url %}
                    <a class="nav-link active" href="{{ link }}">
                        <span class="fa fa-{{ text['icon'] }}"></span> {{ text['text'] }}
                    </a>
                {% else %}
                    <a class="nav-link" href="{{ link }}">
                        <span class="fa fa-{{ text['icon'] }}"></span> {{ text['text'] }}
                    </a>
                {% endif %}
                {% endfor %}
            </li>
        </ul>
    </div>
</div>