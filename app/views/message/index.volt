<div align="left">
    <h2>Zprávy s Filip Macek</h2>
</div>
<hr>
<div class="row">
    <div class="col-md-8">
        <div class="row">

            <div class="col-md-2 text-center">
                <img src="https://vistana-web-static.s3.amazonaws.com/vistana-web/assets/img/profile/production/profile-pic-medium.png?1538246490"
                     class="rounded-circle" style="width: 100%"><br>
                <span class="badge badge-light">17:48</span>
            </div>
            <div class="col-md-10 alert alert-primary text-justify">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi pellentesque arcu diam, vitae imperdiet
                arcu pellentesque vel. Nunc sodales feugiat libero, vitae fringilla enim vulputate sit amet. Vestibulum
                rhoncus finibus lorem convallis convallis. Interdum et malesuada fames ac ante ipsum primis in faucibus.
                Vestibulum malesuada in lectus vel mattis. Sed vitae congue mi. Suspendisse eu nisl justo. Aliquam risus
                augue, rutrum a mattis vel, suscipit vel risus.
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-8 offset-md-4">
        <div class="row">

            <div class="col-md-10 alert alert-success">
                Quisque efficitur nulla nec tincidunt imperdiet. Fusce euismod lectus eget interdum auctor. Nullam a
                mauris quis lacus pharetra ultrices. Maecenas elementum, augue et pellentesque rutrum, arcu sem vehicula
                felis, in molestie nibh nisi quis eros. Nullam sit amet vestibulum libero, eu luctus nibh. Donec a massa
                tortor. Fusce lacinia, ex sit amet bibendum congue, quam libero vestibulum libero, in vulputate lectus
                nunc sed augue. Phasellus tempor hendrerit justo, posuere volutpat ex facilisis nec. Integer quis
                aliquam sem, ut posuere mi. Quisque sed aliquam justo, id congue mauris.
            </div>
            <div class="col-md-2 text-center">
                <img src="https://d2x5ku95bkycr3.cloudfront.net/App_Themes/Common/images/profile/0_200.png"
                     class="rounded-circle" style="width: 100%"><br>
                <span class="badge badge-light">15:21</span>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <textarea class="form-control" rows="3" placeholder="Napište zprávu..."></textarea>
            <div class="float-right">
                <button type="submit" class="btn btn-primary "><i class="fa fa-comment"></i> Poslat zprávu</button>
            </div>
        </div>
    </div>
</div>
