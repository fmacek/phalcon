<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
    <a class="navbar-brand" href="/">Friends wall</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse"
            aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarCollapse">
        <ul class="navbar-nav mr-auto">
            {% set menus = [
                'Zprávy': 'message',
                'Přátelé': 'profile/fmacek/friends'
            ] %}

            {%- for key, value in menus %}
                {% if value == dispatcher.getControllerName() %}
                    <li class="nav-item active">{{ link_to(value, key, 'class':'nav-link') }}</li>
                {% else %}
                    <li class="nav-item">{{ link_to(value, key, 'class':'nav-link') }}</li>
                {% endif %}
            {% endfor %}
        </ul>
        <ul class="navbar-nav mt-2 mt-md-0">
            <li class="dropdown">
                <a href="#" class="dropdown-toggle btn btn-info" data-toggle="dropdown"
                   style="margin-right: 10px">{{ auth.getName() }} <b class="caret"></b></a>
                <ul class="dropdown-menu">
                    <li class="text-center">{{ link_to('users/changePassword', 'Change Password') }}</li>
                    <li class="text-center">{{ link_to('users/changePassword', 'Settings') }}</li>
                </ul>
            </li>
            <li>{{ link_to('session/logout', 'Logout', 'class':'btn btn-danger') }}</li>

        </ul>
    </div>
</nav>
