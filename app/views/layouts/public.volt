<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
    <a class="navbar-brand" href="#">Friends wall</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse"
            aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarCollapse">
        <ul class="navbar-nav mr-auto">


            {%- set menus = [
                'O projektu': 'about',
                'Dokumentace': 'documentation'
            ] -%}
            {%- for key, value in menus %}
                {% if value == dispatcher.getControllerName() %}
                    <li class="nav-item active">{{ link_to(value, key, 'class':'nav-link') }}</li>
                {% else %}
                    <li class="nav-item">{{ link_to(value, key, 'class':'nav-link') }}</li>
                {% endif %}
            {%- endfor -%}
        </ul>
        <ul class="navbar-nav mt-2 mt-md-0">
            {%- if logged_in is defined and not(logged_in is empty) -%}
                <li>{{ link_to('users', 'Users Panel', 'class':'btn btn-info', 'style' : 'margin-right: 10px;') }}</li>
                <li>{{ link_to('session/logout', 'Logout', 'class':'btn btn-danger') }}</li>
            {% else %}
                <li>{{ link_to('session/login', 'Login', 'class':'btn btn-success') }}</li>
            {% endif %}
        </ul>
    </div>
</nav>

<div class="container main-container" style="margin-top: 70px; margin-bottom: 70px;">
    {{ content() }}
</div>

<footer class="footer">
    <div class="container">
        <span class="text-muted">© {% if date("Y") != 2018 %}2018 - {% endif %}{{ date("Y") }} {{ link_to("//filipmacek.cz", "Filip Macek", "target" : "_blank") }}</span>
    </div>
</footer>