<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Welcome to App</title>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
            integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
            integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
            crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
            integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
            crossorigin="anonymous"></script>
    {{ stylesheet_link('inc/css/style.min.css') }}
    {{ stylesheet_link('inc/css/font-awesome.min.css') }}
    {{ stylesheet_link('inc/css/bootstrap.min.css') }}
    {{ javascript_include('inc/js/font-awesome.min.js') }}
    {{ javascript_include('inc/js/jquery.min.js') }}

</head>
<body>

{% include 'layouts/menu.volt' %}


<div class="container main-container" style="margin-top: 70px; margin-bottom: 70px;">
    {{ content() }}
</div>
<footer class="footer">
    <div class="container">
        <span class="text-muted">© {% if date("Y") != 2018 %}2018 - {% endif %}{{ date("Y") }} {{ link_to("//filipmacek.cz", "Filip Macek", "target" : "_blank") }}</span>
    </div>
</footer>
</body>
</html>